Flobbies and Schnops: A Game of Logic  
Created by Luna McNulty - June 2017

With two words you've never heard before, practice logical deduction, hypothesis formation, and classification in a free-as-in-freedom game for Android and the Desktop.

`/public` contains the files availible from the web page  
`/source` contains the LibGDX project files and assets referenced in the code. Build from here.  
`/design` contains files relevant to the project but not necessary for building  
LICENSE contains the GPL3 license for this project

Building requires a working JRE and Gradle. To build for desktop:
 
    cd source
    ./gradlew desktop:dist  

Output will be found in `/source/desktop/build/libs`.

For android:

    cd source
    ./gradlew android:assembleDebug

Output will be found in `/source/android/build/outputs/apk`

For web:

    cd source
    ./gradlew html:dist

Output will be found in `/source/html/build/dist`

For more information see the [LibGDX Deploy Instructions](https://github.com/libgdx/libgdx/wiki/Deploying-your-application). Additional gradle tasks can be viewed with `./gradlew tasks'