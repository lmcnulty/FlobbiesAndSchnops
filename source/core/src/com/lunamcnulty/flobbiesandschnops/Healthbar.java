package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.particles.ResourceData;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by luna on 6/16/17.
 */

public class Healthbar {

    NinePatch bar;
    float marginx;
    float marginy;
    float health;
    float barmarginx;
    float barmarginy;

    public Healthbar(){
        bar = new NinePatch(Assets.instance.hudAssets.panel,72,72,72,72);
        marginx = 400;
        marginy = 25;
        barmarginx = 30;
        barmarginy = 30;
        health = 1;
    }

    public void render(SpriteBatch batch,ShapeRenderer shapeRenderer, float delta, Viewport viewport){
        batch.begin();
        float worldwidth = viewport.getWorldWidth();
        float worldheight = viewport.getWorldHeight();
        float startx = -worldwidth/2 + marginx;
        float starty = -Constants.HEALTH_BAR_HEIGHT - marginy + worldheight/2;
        float barWidth = worldwidth - 2*marginx;

        bar.draw(batch,startx,starty,barWidth,Constants.HEALTH_BAR_HEIGHT);
        batch.end();
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.GREEN);
        shapeRenderer.rect(
		startx + barmarginx,
		starty + barmarginy,
		(barWidth-2*barmarginx)*health,
                Constants.HEALTH_BAR_HEIGHT-2*(barmarginy)
	);
        shapeRenderer.end();
    }
}
