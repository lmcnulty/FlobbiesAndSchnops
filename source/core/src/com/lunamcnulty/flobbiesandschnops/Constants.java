package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by luna on 6/9/17.
 */

public class Constants {

    public static final int WORLD_SIZE = 2560;
    public static final int WORLD_MARGINS = 200;
    public static final float BAR_WIDTH_DENOM = 20;
    public static final Vector2 BAR_DIMENSIONS = new Vector2((Constants.WORLD_SIZE-Constants.WORLD_MARGINS)/BAR_WIDTH_DENOM,Constants.WORLD_SIZE);
    public static final Vector2 BAR_POSITION = new Vector2(((Constants.WORLD_SIZE-Constants.WORLD_MARGINS)/2),-BAR_DIMENSIONS.y/2);
    public static final Preferences prefs = Gdx.app.getPreferences("Flobbies and Schnops Save Data");
    public static final float HEALTH_BAR_HEIGHT = 150;
    public static final float FLOBBY_WIDTH = 256;
    public static final RandomDialogue RANDOM_DIALOGUE = new RandomDialogue();
}
