package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by luna on 6/15/17.
 */

public class Menubutton {

    Vector2 position;
    Vector2 dimensions;
    float textPadding;
    BitmapFont font;
    GlyphLayout layout;
    ShapeRenderer shapeRenderer;
    String text;
    Color color;
    Rectangle hitRect;

    public Menubutton(Vector2 position, Vector2 dimensions, String text){
        font = new BitmapFont(Gdx.files.internal("cantarell-mono.fnt"),false);
        font.setColor(Color.WHITE);
        font.getData().setScale(2);
        shapeRenderer = new ShapeRenderer();
        color = Color.GRAY;
        shapeRenderer.setColor(color);
        this.position = position;
        this.dimensions = dimensions;
        this.text = text;
        textPadding = 40;
        layout = new GlyphLayout();
        layout.setText(font,this.text);
        float width = layout.width + 2*textPadding;
        float height = font.getLineHeight() + textPadding;
        if(dimensions.x < width){
            dimensions.x = width;
        }
        if(dimensions.y < height){
            dimensions.y = height;
        }
        hitRect = new Rectangle(position.x - dimensions.x/2,position.y - dimensions.y/2,dimensions.x,dimensions.y);
    }
    public void render(SpriteBatch batch, float delta){
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.setColor(color);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.rect(position.x - dimensions.x/2,position.y - dimensions.y/2,dimensions.x,dimensions.y);
        hitRect = new Rectangle(position.x - dimensions.x/2,position.y - dimensions.y/2,dimensions.x,dimensions.y);
        shapeRenderer.end();
        batch.begin();
        font.draw(batch,text,position.x - layout.width/2,position.y + font.getLineHeight()/4);
        batch.end();
    }
    public void dispose(){
        shapeRenderer.dispose();
        font.dispose();
    }
}
