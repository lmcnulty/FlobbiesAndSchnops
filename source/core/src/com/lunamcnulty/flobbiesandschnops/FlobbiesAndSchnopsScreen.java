package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import java.util.HashSet;
import static com.badlogic.gdx.math.MathUtils.random;

/**
 * Created by luna on 6/6/17.
 */


public class FlobbiesAndSchnopsScreen extends InputAdapter implements Screen{

    Preferences prefs = Constants.prefs;
    ExtendViewport viewport;
    SpriteBatch batch;
    Array<FlobbyOrSchnop> flobbiesAndSchnops;
    ShapeRenderer shapeRenderer;
    FlobbyOrSchnopGenerator generator;
    Dialogue currentDialogue;
    Sprite background;
    Button button;
    Button buttonMenu;
    Level level;
    HashSet<FlobbyOrSchnop> flobbies;
    HashSet<FlobbyOrSchnop> schnops;
    boolean victoryFlag;
    boolean exitFlag;
    int attempts;
    BitmapFont font;
    GlyphLayout layout;
    FlobbiesAndSchnops game;
    FlobbyOrSchnop holding;
    Vector2 touchDistance;
    Healthbar healthbar;
    float healthDecrease;
    int startlevel;
    Music music;
    Dialogue exitDialogue;

    public FlobbiesAndSchnopsScreen(FlobbiesAndSchnops game, int startlevel){
        this.game = game;
        this.startlevel = startlevel;
    }
    @Override
    public void show(){
        Gdx.input.setCatchBackKey(true);
        healthDecrease = .1f;
        font = new BitmapFont(Gdx.files.internal("cantarell-mono.fnt"),false);
        layout = new GlyphLayout();
        font.setColor(Color.WHITE);
        font.getData().setScale(3f);
        attempts = 0;
        victoryFlag = false;
        exitFlag = false;
        music = Gdx.audio.newMusic(Gdx.files.internal("music.ogg"));
        if(prefs.getBoolean("sound", true) == true) {
            music.setLooping(true);
            music.play();
        }
        AssetManager am = new AssetManager();
        Assets.instance.init(am);
        viewport = new ExtendViewport(Constants.WORLD_SIZE,Constants.WORLD_SIZE,3*Constants.WORLD_SIZE,2*Constants.WORLD_SIZE);
        level = new Level(startlevel-1,viewport);
        currentDialogue = level.introSpeech;
        background = new Sprite(new Texture(Gdx.files.internal("background.png")));
        background.setCenter(0,0);
        background.setScale(4);
        shapeRenderer = new ShapeRenderer();
        generator = level.generator;
        batch = new SpriteBatch();
        flobbiesAndSchnops = new Array<FlobbyOrSchnop>();
        button = new Button(viewport,-1,Assets.instance.hudAssets.submit);
        buttonMenu = new Button(viewport,1,Assets.instance.hudAssets.menu);
        flobbies = new HashSet<FlobbyOrSchnop>();
        schnops = new HashSet<FlobbyOrSchnop>();
        Gdx.input.setInputProcessor(this);
        healthbar = new Healthbar();
        exitDialogue = new Dialogue(viewport,"Are you sure you want to quit and return to the main menu?",2);
        exitDialogue.options[0] = "Yes";
        exitDialogue.options[1] = "No";
        this.startNextLevel();
    }

    @Override
    public void render (float delta) {
        if(Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) && Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT) && Gdx.input.isKeyJustPressed(Input.Keys.S)){
            this.startNextLevel();
        }
        if(exitDialogue.getAnswer() == 0){
            exitFlag = true;
        }
        if(exitFlag && currentDialogue.isFinished()){
            this.exitLevel();
        }
        viewport.apply();
        batch.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        background.draw(batch);
        batch.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(Constants.BAR_POSITION.x-(Constants.BAR_DIMENSIONS.x/2),viewport.getWorldHeight()/-2,Constants.BAR_DIMENSIONS.x,viewport.getWorldHeight());
        shapeRenderer.setColor(Color.BLUE);
        shapeRenderer.rect(-Constants.BAR_POSITION.x-(Constants.BAR_DIMENSIONS.x/2),viewport.getWorldHeight()/-2,Constants.BAR_DIMENSIONS.x,viewport.getWorldHeight());
        shapeRenderer.end();

        batch.begin();
        for(FlobbyOrSchnop flobbyOrSchnop: flobbiesAndSchnops){
            flobbyOrSchnop.render(batch,delta);
        }
        button.render(batch,delta);
        buttonMenu.render(batch,delta);
        currentDialogue.render(batch,delta);
        layout.setText(font,Integer.toString(attempts));
        batch.end();

        healthbar.render(batch,shapeRenderer,delta,viewport);
        if(victoryFlag){
            if(currentDialogue.menu.answered){
                boolean finished = true;
                for (FlobbyOrSchnop e: flobbiesAndSchnops){
                    if (e.position.x != e.destination.x){
                        finished = false;
                    }
                }
                if(finished){
                    this.startNextLevel();
                }
            }
        }
    }

    public void submitAnswer(){
        attempts +=1;
        boolean correct = true;
        boolean complete = true;
        Array<FlobbyOrSchnop> nonFlobbies = level.getIncorrectFlobbies(flobbies);
        Array<FlobbyOrSchnop> nonSchnops = level.getIncorrectSchnops(schnops);
        if (nonFlobbies.size > 0 && nonSchnops.size == 0){
            correct = false;
            currentDialogue = new Dialogue(viewport,level.wrongFlobbies,0);
        }
        else if(nonSchnops.size > 0 && nonFlobbies.size == 0){
            correct = false;
            currentDialogue = new Dialogue(viewport,level.wrongSchnops,0);
        }
        else if(nonSchnops.size > 0 && nonFlobbies.size > 0){
            correct = false;
            currentDialogue = new Dialogue(viewport,level.wrongEverything,0);
        }else{
            for(FlobbyOrSchnop e: flobbiesAndSchnops){
                if(!flobbies.contains(e) && !schnops.contains(e)) {
                    if (level.isItAFlobby(e) || level.isItASchnop(e)) {
                        correct = false;
                        complete = false;
                    }
                }
            }
            if (!complete){
                currentDialogue = new Dialogue(viewport,level.incomplete,0);
            }
        }
        if(!correct){
            if(healthbar.health > 0){
                healthbar.health -= level.healthDecrement;
            }
            if(healthbar.health <= 0){
                currentDialogue = new Dialogue(viewport,level.failureMessage,0);
                exitFlag = true;
            }
        }else{
            currentDialogue = level.outroSpeech;
            for (FlobbyOrSchnop e: flobbiesAndSchnops){
                e.destination = new Vector2(0,-Constants.WORLD_SIZE);
            }
            victoryFlag = true;

        }
    }

    public void startNextLevel(){
        attempts = 0;
        victoryFlag = false;
        flobbiesAndSchnops.clear();
        flobbies.clear();
        schnops.clear();
        level = new Level(level.levelNumber+1,viewport);
        healthbar.health = 1;
        if(level.levelNumber > prefs.getInteger("level")) {
            prefs.putInteger("level", level.levelNumber);
            prefs.flush();
        }
        generator = level.generator;
        for(int i = 0; i < 20; i++){
            FlobbyOrSchnop newcomer = generator.generate();

            newcomer.setPositionY(random(-(Constants.WORLD_SIZE/2-Constants.WORLD_MARGINS),(Constants.WORLD_SIZE/2-Constants.WORLD_MARGINS-2*Constants.HEALTH_BAR_HEIGHT)));
            newcomer.setPositionX(random(-(Constants.WORLD_SIZE/2-Constants.WORLD_MARGINS),(Constants.WORLD_SIZE/2-Constants.WORLD_MARGINS)-Constants.FLOBBY_WIDTH));
            newcomer.calculateHitRect();
            newcomer.calculateShadowOffset();
            flobbiesAndSchnops.add(newcomer);
        }
        currentDialogue = level.introSpeech;
    }
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Vector2 touchPosition = viewport.unproject(new Vector2(screenX,screenY));
        MenuFrame relaventMenu = currentDialogue.getMenu();
        if(relaventMenu!=null){
            Rectangle hitRect = new Rectangle(relaventMenu.position.x,relaventMenu.position.y,relaventMenu.dimensions.x,relaventMenu.dimensions.y);
            if(hitRect.contains(touchPosition) && relaventMenu.options.length > 0){
                relaventMenu.choice = (relaventMenu.choice+1)%relaventMenu.options.length;
            }else{
                currentDialogue.proceed();
            }
        }else {
            currentDialogue.proceed();
        }
        if (holding == null){
            for (FlobbyOrSchnop e: flobbiesAndSchnops){
                if (e.hitRect.contains(touchPosition.x,touchPosition.y)){
                    holding = e;

                    touchDistance = new Vector2(touchPosition.x - e.position.x,touchPosition.y - e.position.y);
                }
            }
            if(holding != null){
                flobbiesAndSchnops.removeValue(holding, true);
                flobbiesAndSchnops.add(holding);
            }
        }else{
            holding.setPosition(new Vector2(touchPosition.x-touchDistance.x,touchPosition.y-touchDistance.y));
            holding.calculateShadowOffset();
            holding.calculateHitRect();
        }
        if(this.button.hitRect.contains(touchPosition)){
            submitAnswer();
        }else if(this.buttonMenu.hitRect.contains(touchPosition)){
            this.showExitDialogue();
        }
        return true;
    }

    public void showExitDialogue(){
        exitDialogue = new Dialogue(viewport,"Are you sure you want to quit and return to the main menu?",2);
        exitDialogue.options[0] = "Yes";
        exitDialogue.options[1] = "No";
        currentDialogue = exitDialogue;
    }

    public void exitLevel(){
        music.stop();
        game.showLaunchScreen();
    }
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer){
        if(holding != null) {
            Vector2 touchPosition = viewport.unproject(new Vector2(screenX, screenY));
            holding.setPosition(new Vector2(touchPosition.x - touchDistance.x, touchPosition.y - touchDistance.y));
            holding.calculateShadowOffset();
            holding.calculateHitRect();
        }
        return true;
    }
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {

        if(holding != null){
            Vector2 touchPosition = viewport.unproject(new Vector2(screenX, screenY));
            if(touchPosition.x < -Constants.BAR_POSITION.x-(Constants.BAR_DIMENSIONS.x/2)){
                if (!flobbies.contains(holding)){
                    flobbies.add(holding);
                }
                if(schnops.contains(holding)){
                    schnops.remove(holding);
                }
            }else if(touchPosition.x > Constants.BAR_POSITION.x-(Constants.BAR_DIMENSIONS.x/2)){
                if (!schnops.contains(holding)){
                    schnops.add(holding);
                }
                if(flobbies.contains(holding)){
                    flobbies.remove(holding);
                }
            }else{
                if(flobbies.contains(holding)){
                    flobbies.remove(holding);
                }
                if(schnops.contains(holding)){
                    schnops.remove(holding);
                }
            }
        }
        holding = null;
        return true;
    }

    @Override
    public boolean keyDown(int keyCode){
        if(keyCode == Input.Keys.BACK|| keyCode == Input.Keys.MENU){
            this.showExitDialogue();
        }
        return true;
    }

    @Override
    public void pause(){}
    @Override
    public void resume(){}

    @Override
    public void hide(){}

    @Override
    public void resize(int width, int height){
        viewport.update(width,height);
    }

    @Override
    public void dispose () {
        shapeRenderer.dispose();
        batch.dispose();
    }
}
