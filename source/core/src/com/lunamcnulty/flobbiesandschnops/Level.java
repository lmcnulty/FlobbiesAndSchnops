package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Math.random;

/**
 * Created by luna on 6/10/17.
 */

public class Level {

    FlobbyOrSchnopGenerator generator;
    boolean[] requiredFlobbyFeatures;
    boolean[] requiredSchnopFeatures;
    int levelNumber;
    String failureMessage;
    String wrongFlobbies;
    String wrongSchnops;
    String wrongEverything;
    String incomplete;
    Dialogue introSpeech;
    Dialogue outroSpeech;
    float healthDecrement; // The easier the challenge, the higher this should be
    int Min;
    int Max;

    /* 0  = round
     * 1  = mixed
     * 2  = blocky
     * 3  = spotted
     * 4  = spotless
     * 5  = banded
     * 6  = bandless
     * 7  = shadowed
     * 8  = shadowless
     * 9  = shiny
     * 10 = shineless
     * 11 = striped
     * 12 = stripeless
     * 13 = crest
     * 14 = crestless
     * 15 = green
     * 16 = purple
     */

    public Level(int number, Viewport viewport){
        failureMessage = "Uh oh, you're out of guesses.";
        wrongFlobbies = "That's no Flobby.";
        wrongSchnops = "That's no Schnop.";
        wrongEverything = "Not a Flobby. Not a Schnop.";
        incomplete = "Something's missing";
        outroSpeech = new Dialogue(viewport,"Very good job.",0);
        introSpeech = new Dialogue(viewport,"Level up.",0);
        generator = new FlobbyOrSchnopGenerator();
        requiredFlobbyFeatures = new boolean[17];
        requiredSchnopFeatures = new boolean[17];
        for (int i = 0; i < 17; i++){
            requiredFlobbyFeatures[i] = false;
            requiredSchnopFeatures[i] = false;
        }
        this.levelNumber = number;
        healthDecrement = .1f;

        switch(levelNumber){
            case 1:
                healthDecrement = .07f;
                requiredFlobbyFeatures[2] = true;
                requiredSchnopFeatures[0] = true;
                this.greenAndPurpleProtoTypes(1);
                introSpeech = new Dialogue(viewport, "Welcome to the game.",1);
                Dialogue introSpeech2 = new Dialogue(viewport, "Would you like me to explain the rules?",2);
                Dialogue introSpeech3 = new Dialogue(viewport,"Your task is easy.",1);
                Dialogue introSpeech4 = new Dialogue(viewport, "Simply put the flobbies behind the blue line, and the Schnops behind the left.",1);
                Dialogue introSpeech5 = new Dialogue(viewport, "To check your answer, press the button on the top-left corner.",1);
                Dialogue introSpeech7 = new Dialogue(viewport,"You have a limited number of guesses. Don't let the bar at the top run out!",1);
                Dialogue introSpeech6 = new Dialogue(viewport, "No time for questions. Begin!",0);

                introSpeech.options[0] = "";
                introSpeech.followUp[0] = introSpeech2;

                introSpeech2.options[0] = "Yes.";
                introSpeech2.options[1] = "No.";
                introSpeech2.followUp[0] = introSpeech3;

                introSpeech3.options[0] = "";
                introSpeech3.followUp[0] = introSpeech4;

                introSpeech4.options[0] = "";
                introSpeech4.followUp[0] = introSpeech5;

                introSpeech5.options[0] = "";
                introSpeech5.followUp[0] = introSpeech7;

                introSpeech7.options[0] = "";
                introSpeech7.followUp[0] = introSpeech6;

                break;
            case 2:
                healthDecrement = .07f;
                requiredFlobbyFeatures[2] = true;
                requiredSchnopFeatures[0] = true;
                this.greenAndPurpleProtoTypes(.8f);
                introSpeech = new Dialogue(viewport,"Let's do this again.",0);
                break;
            case 3:
                healthDecrement = .07f;
                requiredSchnopFeatures[2] = true;
                requiredFlobbyFeatures[0] = true;
                this.greenAndPurpleProtoTypes(.9f);
                introSpeech = new Dialogue(viewport,"Assume nothing.",0);
                break;
            case 4:
                requiredSchnopFeatures[3] = true;
                requiredFlobbyFeatures[11] = true;
                this.greenAndPurpleProtoTypes(.7f);
                introSpeech = new Dialogue(viewport,"Observe every detail.",0);
                break;
            case 5:
                requiredFlobbyFeatures[2] = true;
                requiredFlobbyFeatures[14] = true;
                requiredSchnopFeatures[0] = true;
                requiredSchnopFeatures[14] = true;
                this.greenAndPurpleProtoTypes(.9f);
                generator.correlations[0][13] = .4f;
                generator.correlations[2][13] = .4f;
                generator.correlations[1][13] = .4f;
                introSpeech = new Dialogue(viewport,"Nothing is something.",0);
                break;
            case 6:
                this.greenAndPurpleProtoTypes(.9f);
                requiredFlobbyFeatures[0] = true;
                requiredFlobbyFeatures[5] = true;
                requiredSchnopFeatures[2] = true;
                requiredSchnopFeatures[12] = true;
                introSpeech = new Dialogue(viewport,"Don't be single-minded.",0);
                break;
            case 7:
                requiredSchnopFeatures[0] = true;
                requiredSchnopFeatures[13] = true;
                requiredFlobbyFeatures[2] = true;
                requiredFlobbyFeatures[13] = true;
                this.greenAndPurpleProtoTypes(.9f);
                generator.correlations[0][13] = .3f;
                generator.correlations[2][13] = .3f;
                introSpeech = new Dialogue(viewport,"It's OK to be in the minority.",0);
                break;
            case 8:
                requiredFlobbyFeatures[7] = true;
                requiredSchnopFeatures[8] = true;
                this.greenAndPurpleProtoTypes(.7f);
                generator.correlations[2][8] = .4f;
                introSpeech = new Dialogue(viewport,"Important clues don't always stand out.",0);
                break;
            case 9:
                requiredSchnopFeatures[5] = true;
                requiredSchnopFeatures[9] = true;
                requiredFlobbyFeatures[2] = true;
                this.greenAndPurpleProtoTypes(.9f);
                introSpeech = new Dialogue(viewport,"No hints this time.",0);
                break;
            case 10:
                Min = 0;
                Max = 16;
                int roll = Min + (int)(Math.random() * ((Max - Min) + 1));
                requiredFlobbyFeatures[roll] = true;
                requiredSchnopFeatures[generator.getOpposite(roll)] = true;

                this.greenAndPurpleProtoTypes(.7f);
                introSpeech = new Dialogue(viewport,"Take note: features are either allowed or forbidden. Whether a feature is allowed does not affect whether another is or is not.",0);
                break;
            case 11:
                requiredSchnopFeatures[1] = true;
                requiredFlobbyFeatures[1] = true;
                Min = 3;
                Max = 16;
                roll = Min + (int)(Math.random() * ((Max - Min) + 1));
                int roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                while(roll2 == roll || roll2 == generator.getOpposite(roll)){
                    roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                requiredFlobbyFeatures[roll] = true;
                requiredSchnopFeatures[roll2] = true;
                introSpeech = new Dialogue(viewport,"Bigger is not always better.",0);
                break;
            case 12:
                requiredFlobbyFeatures[9] = true;
                requiredSchnopFeatures[10] = true;
                generator.correlations[5][9] = .9f;
                generator.correlations[5][1] = .9f;
                generator.correlations[5][7] = .9f;
                generator.correlations[5][12] = .9f;
                generator.correlations[6][10] = .9f;
                generator.correlations[6][0] = .9f;
                generator.correlations[6][8] = .9f;
                generator.correlations[6][11] = .9f;
                generator.correlations[6][9] = .1f;
                generator.correlations[5][10] = .1f;
                introSpeech = new Dialogue(viewport,"Learn to adapt.",0);
                break;
            case 13:
                healthDecrement = .05f;
                requiredFlobbyFeatures[1] = true;
                requiredFlobbyFeatures[3] = true;
                requiredFlobbyFeatures[11] = true;
                requiredSchnopFeatures[15] = true;
                requiredSchnopFeatures[5] = true;
                requiredSchnopFeatures[10] = true;
                introSpeech = new Dialogue(viewport,"A needle in a haystack.",0);
                this.greenAndPurpleProtoTypes(.5f);
                break;
            case 14:
                requiredSchnopFeatures[15] = true;
                requiredFlobbyFeatures[11] = true;
                generator.baseProportions[0] = .9f;
                generator.baseProportions[1] = .05f;
                generator.baseProportions[2] = .05f;
                generator.correlations[0][0] = .9f;
                generator.correlations[0][1] = 0;
                generator.correlations[0][2] = 0;
                generator.correlations[0][3] = 0;
                generator.correlations[0][4] = .9f;
                generator.correlations[0][5] = .9f;
                generator.correlations[0][6] = 0;
                generator.correlations[0][7] = 0;
                generator.correlations[0][8] = .9f;
                generator.correlations[0][9] = .9f;
                generator.correlations[0][10] = 0;
                generator.correlations[0][11] = .9f;
                generator.correlations[0][12] = 0;
                generator.correlations[0][13] = 0;
                generator.correlations[0][14] = .9f;
                introSpeech = new Dialogue(viewport,"Some hay in a needle stack?",0);
                break;
            case 15:
                generator.baseProportions[8] = .2f;
                generator.correlations[13][5] = 1;
                generator.correlations[5][7] = .7f;
                requiredSchnopFeatures[5] = true;
                requiredFlobbyFeatures[8] = true;
                introSpeech = new Dialogue(viewport,"Why are they called Flobbies anyway?",0);
                break;
            case 16:
                this.greenAndPurpleProtoTypes(.7f);
                Min = 1;
                Max = 16;
                roll = Min + (int)(Math.random() * ((Max - Min) + 1));
                roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                int roll3 = Min + (int)(Math.random() * ((Max - Min) + 1));
                int roll4 = Min + (int)(Math.random() * ((Max - Min) + 1));
                while(roll2 == roll){
                    roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                while(roll3 == roll || roll3 == roll2){
                    roll3 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                while(roll4 == roll || roll4 == roll2 || roll4 == roll3){
                    roll4 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                requiredFlobbyFeatures[roll] = true;
                requiredSchnopFeatures[roll2] = true;
                requiredFlobbyFeatures[roll3] = true;
                requiredSchnopFeatures[roll4] = true;
                introSpeech = new Dialogue(viewport,"The dictionary says that 'to flob' means 'to be clumsy or aimless in moving.",1);
                introSpeech.followUp[0] = new Dialogue(viewport,"That doesn't sound quite right.",0);
                break;


            case 17:
                for (int i = 0; i < 16; i++){
                    requiredFlobbyFeatures[i] = false;
                    requiredSchnopFeatures[i] = false;
                }
                Min = 0;
                Max = 16;
                roll = Min + (int)(Math.random() * ((Max - Min) + 1));
                roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                while(roll2 == roll){
                    roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                requiredFlobbyFeatures[roll] = true;
                introSpeech = new Dialogue(viewport,"Everything you know is wrong.",0);
                break;
            case 18:
                for (int i = 0; i < 16; i++){
                    requiredFlobbyFeatures[i] = false;
                    requiredSchnopFeatures[i] = false;
                }
                requiredFlobbyFeatures[0] = true;
                requiredFlobbyFeatures[3] = true;
                requiredFlobbyFeatures[6] = true;
                requiredFlobbyFeatures[13] = true;
                this.greenAndPurpleProtoTypes(.9f);
                generator.correlations[15][3] = .4f;
                generator.correlations[15][13] = .4f;
                generator.correlations[3][13] = .4f;
                generator.correlations[13][6] = .4f;
                introSpeech = new Dialogue(viewport,"Don't over-complicate things.",0);
                break;
            case 19:
                for (int i = 0; i < 16; i++){
                    requiredFlobbyFeatures[i] = false;
                    requiredSchnopFeatures[i] = false;
                }
                requiredFlobbyFeatures[0] = false;
                requiredSchnopFeatures[0] = true;
                requiredFlobbyFeatures[13] = true;
                requiredSchnopFeatures[13] = false;
                requiredFlobbyFeatures[5] = false;
                requiredSchnopFeatures[5] = true;
                this.greenAndPurpleProtoTypes(.4f);
                generator.correlations[0][8] = 1f;
                introSpeech = new Dialogue(viewport,"This one is Hard.",0);
                break;
            case 20:
                requiredSchnopFeatures[7] = true;
                requiredFlobbyFeatures[7] = true;
                requiredFlobbyFeatures[8] = true;
                generator.correlations[15][16] = 1;
                generator.correlations[16][0] = 0;
                generator.correlations[16][1] = 0;
                generator.correlations[16][2] = .9f;
                generator.correlations[16][3] = .9f;
                generator.correlations[16][4] = 0;
                generator.correlations[16][5] = 0;
                generator.correlations[16][6] = .9f;
                generator.correlations[16][7] = .9f;
                generator.correlations[16][8] = 0;
                generator.correlations[16][9] = 0;
                generator.correlations[16][10] = .9f;
                generator.correlations[16][11] = 0;
                generator.correlations[16][12] = .9f;
                generator.correlations[16][13] = 0;
                generator.correlations[16][14] = .9f;
                introSpeech = new Dialogue(viewport,"On strike.",0);
                break;
            case 21:
                Min = 0;
                Max = 16;
                roll = Min + (int)(Math.random() * ((Max - Min) + 1));
                roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                while(roll2 == roll){
                    roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                requiredFlobbyFeatures[roll] = true;
                requiredSchnopFeatures[roll2] = true;
                for(int i = 0; i < 17; i++){
                    for(int j = 0; j< 17; j++){
                        generator.correlations[i][j] = (float)random();
                    }
                }
                generator.correlations[0][8] = 1;
                introSpeech = new Dialogue(viewport,"This level is mostly randomly generated. More than usual, I mean.",0);
                break;
            case 22:
                generator.correlations[0][8] = 1f;
                requiredFlobbyFeatures[0] = true;
                requiredFlobbyFeatures[1] = true;
                requiredSchnopFeatures[0] = true;
                requiredSchnopFeatures[1] = true;
                introSpeech = new Dialogue(viewport,"You must be tired.",0);
                outroSpeech = new Dialogue(viewport,"See? Nothing to do.",0);
                break;
            case 23:
                generator.correlations[15][0] = .9f;
                generator.correlations[15][1] = 0;
                generator.correlations[15][2] = 0;
                generator.correlations[15][3] = 0;
                generator.correlations[15][4] = .9f;
                generator.correlations[15][5] = .9f;
                generator.correlations[15][6] = 0;
                generator.correlations[15][7] = 0;
                generator.correlations[15][8] = .9f;
                generator.correlations[15][9] = .9f;
                generator.correlations[15][10] = 0;
                generator.correlations[15][11] = .9f;
                generator.correlations[15][12] = 0;
                generator.correlations[15][13] = 0;
                generator.correlations[15][14] = .9f;


                requiredSchnopFeatures[0] = true;
                requiredSchnopFeatures[1] = false;
                requiredSchnopFeatures[2] = false;
                requiredSchnopFeatures[3] = false;
                requiredSchnopFeatures[4] = true;
                requiredSchnopFeatures[5] = true;
                requiredSchnopFeatures[6] = false;
                requiredSchnopFeatures[7] = false;
                requiredSchnopFeatures[8] = true;
                requiredSchnopFeatures[9] = true;
                requiredSchnopFeatures[10] = false;
                requiredSchnopFeatures[11] = true;
                requiredSchnopFeatures[12] = false;
                requiredSchnopFeatures[13] = false;
                requiredSchnopFeatures[14] = true;
                requiredSchnopFeatures[15] = true;
                requiredSchnopFeatures[16] = false;


                generator.correlations[16][0] = 0;
                generator.correlations[16][1] = 0;
                generator.correlations[16][2] = .9f;
                generator.correlations[16][3] = .9f;
                generator.correlations[16][4] = 0;
                generator.correlations[16][5] = 0;
                generator.correlations[16][6] = .9f;
                generator.correlations[16][7] = .9f;
                generator.correlations[16][8] = 0;
                generator.correlations[16][9] = 0;
                generator.correlations[16][10] = .9f;
                generator.correlations[16][11] = 0;
                generator.correlations[16][12] = .9f;
                generator.correlations[16][13] = 0;
                generator.correlations[16][14] = .9f;
                generator.correlations[0][8] = 1f;

                requiredFlobbyFeatures[16] = true;
                requiredFlobbyFeatures[2] = true;
                requiredFlobbyFeatures[3] = true;
                requiredFlobbyFeatures[4] = false;
                requiredFlobbyFeatures[5] = false;
                requiredFlobbyFeatures[6] = true;
                requiredFlobbyFeatures[7] = true;
                requiredFlobbyFeatures[8] = false;
                requiredFlobbyFeatures[9] = false;
                requiredFlobbyFeatures[10] = true;
                requiredFlobbyFeatures[11] = false;
                requiredFlobbyFeatures[12] = true;
                requiredFlobbyFeatures[13] = false;
                requiredFlobbyFeatures[14] = true;

                introSpeech = new Dialogue(viewport,"Time to play police officer.",0);
                break;
            case 24:
                requiredSchnopFeatures[0] = false;
                requiredSchnopFeatures[1] = true;
                requiredSchnopFeatures[2] = false;
                requiredSchnopFeatures[3] = false;
                requiredSchnopFeatures[4] = false;
                requiredSchnopFeatures[5] = true;
                requiredSchnopFeatures[6] = false;
                requiredSchnopFeatures[7] = false;
                requiredSchnopFeatures[8] = false;
                requiredSchnopFeatures[9] = false;
                requiredSchnopFeatures[10] = false;
                requiredSchnopFeatures[11] = false;
                requiredSchnopFeatures[12] = false;
                requiredSchnopFeatures[13] = false;
                requiredSchnopFeatures[14] = true;
                requiredSchnopFeatures[15] = false;
                requiredSchnopFeatures[16] = false;

                requiredFlobbyFeatures[0] = false;
                requiredFlobbyFeatures[1] = false;
                requiredFlobbyFeatures[2] = true;
                requiredFlobbyFeatures[3] = false;
                requiredFlobbyFeatures[4] = false;
                requiredFlobbyFeatures[5] = false;
                requiredFlobbyFeatures[6] = false;
                requiredFlobbyFeatures[7] = false;
                requiredFlobbyFeatures[8] = false;
                requiredFlobbyFeatures[9] = false;
                requiredFlobbyFeatures[10] = false;
                requiredFlobbyFeatures[11] = false;
                requiredFlobbyFeatures[12] = false;
                requiredFlobbyFeatures[13] = false;
                requiredFlobbyFeatures[14] = false;
                requiredFlobbyFeatures[15] = true;
                requiredFlobbyFeatures[16] = false;
                introSpeech = new Dialogue(viewport,"Sorting Flobbies and Schnops.",1);
                introSpeech.options[0] = "";
                introSpeech.followUp[0] = new Dialogue(viewport,"It fills you with determination.",0);
                break;
            case 25:
                introSpeech = new Dialogue(viewport,"Congratulations on making it this far! From this point on, all levels are randomly generated.",0);
                healthDecrement = .07f;
                Min = 0;
                Max = 16;
                roll = Min + (int)(Math.random() * ((Max - Min) + 1));
                roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                roll3 = Min + (int)(Math.random() * ((Max - Min) + 1));
                roll4 = Min + (int)(Math.random() * ((Max - Min) + 1));
                while(roll2 == roll){
                    roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                while(roll3 == roll || roll3 == roll2){
                    roll3 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                while(roll4 == roll || roll4 == roll2 || roll4 == roll3){
                    roll4 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                requiredFlobbyFeatures[roll] = true;
                requiredSchnopFeatures[roll2] = true;
                if(random() > .5){
                    requiredSchnopFeatures[roll4] = true;
                }
                if(random() > .5){
                    requiredFlobbyFeatures[roll3] = true;
                }
                this.greenAndPurpleProtoTypes((float)random());
                for(int i = 0; i < 17; i++){
                    for(int j = 0; j< 17; j++){
                        generator.correlations[i][j] = (float)random();
                    }
                }
                break;
            default:
                introSpeech = new Dialogue(viewport,Constants.RANDOM_DIALOGUE.get(),0);
                healthDecrement = .07f;
                Min = 0;
                Max = 16;
                roll = Min + (int)(Math.random() * ((Max - Min) + 1));
                roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                roll3 = Min + (int)(Math.random() * ((Max - Min) + 1));
                roll4 = Min + (int)(Math.random() * ((Max - Min) + 1));
                while(roll2 == roll){
                    roll2 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                while(roll3 == roll || roll3 == roll2){
                    roll3 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                while(roll4 == roll || roll4 == roll2 || roll4 == roll3){
                    roll4 = Min + (int)(Math.random() * ((Max - Min) + 1));
                }
                requiredFlobbyFeatures[roll] = true;
                requiredSchnopFeatures[roll2] = true;
                if(random() > .5){
                    requiredSchnopFeatures[roll4] = true;
                }
                if(random() > .5){
                    requiredFlobbyFeatures[roll3] = true;
                }
                this.greenAndPurpleProtoTypes((float)random());
                for(int i = 0; i < 17; i++){
                    for(int j = 0; j< 17; j++){
                        generator.correlations[i][j] = (float)random();
                    }
                }
        }
        generator.correlations[0][8] = 1;
        generator.correlations[2][6] = 1;
    }

    public boolean isItAFlobby(FlobbyOrSchnop e){
        if(requiredFlobbyFeatures[0]){
            if(e.bodyType != FlobbyOrSchnop.BodyType.ROUND){
                return false;
            }
        }
        if(requiredFlobbyFeatures[1]){
            if(e.bodyType != FlobbyOrSchnop.BodyType.MIXED){
                return false;
            }
        }
        if(requiredFlobbyFeatures[2]){
            if(e.bodyType != FlobbyOrSchnop.BodyType.BLOCKY){
                return false;
            }
        }
        if(requiredFlobbyFeatures[3]){
            if(!e.spots){
                return false;
            }
        }
        if(requiredFlobbyFeatures[4]){
            if(e.spots){
                return false;
            }
        }
        if(requiredFlobbyFeatures[5]){
            if(!e.band){
                return false;
            }
        }
        if(requiredFlobbyFeatures[6]){
            if(e.band){
                return false;
            }
        }
        if(requiredFlobbyFeatures[7]){
            if(!e.shadows){
                return false;
            }
        }
        if(requiredFlobbyFeatures[8]){
            if(e.shadows){
                return false;
            }
        }
        if(requiredFlobbyFeatures[9]){
            if(!e.shine){
                return false;
            }
        }
        if(requiredFlobbyFeatures[10]){
            if(e.shine){
                return false;
            }
        }
        if(requiredFlobbyFeatures[11]){
            if(!e.striped){
                return false;
            }
        }
        if(requiredFlobbyFeatures[12]){
            if(e.striped){
                return false;
            }
        }
        if(requiredFlobbyFeatures[13]){
            if(!e.crest){
                return false;
            }
        }
        if(requiredFlobbyFeatures[14]){
            if(e.crest){
                return false;
            }
        }
        return true;
    }

    public boolean areThereMissingFlobbiesOrSchnops(Set<FlobbyOrSchnop> input){
        for(FlobbyOrSchnop e:input){
            if(isItASchnop(e) || isItAFlobby(e)){
                return false;
            }
        }
        return true;
    }

    public Array<FlobbyOrSchnop> getIncorrectFlobbies(Set<FlobbyOrSchnop> input){
        Array<FlobbyOrSchnop> output = new Array<FlobbyOrSchnop>();
        for (FlobbyOrSchnop e: input){
            if(!this.isItAFlobby(e)){
                output.add(e);
            }
        }
        return output;
    }

    public boolean isItASchnop(FlobbyOrSchnop e){
        if(requiredSchnopFeatures[0]){
            if(e.bodyType != FlobbyOrSchnop.BodyType.ROUND){
                return false;
            }
        }
        if(requiredSchnopFeatures[1]){
            if(e.bodyType != FlobbyOrSchnop.BodyType.MIXED){
                return false;
            }
        }
        if(requiredSchnopFeatures[2]){
            if(e.bodyType != FlobbyOrSchnop.BodyType.BLOCKY){
                return false;
            }
        }
        if(requiredSchnopFeatures[3]){
            if(!e.spots){
                return false;
            }
        }
        if(requiredSchnopFeatures[4]){
            if(e.spots){
                return false;
            }
        }
        if(requiredSchnopFeatures[5]){
            if(!e.band){
                return false;
            }
        }
        if(requiredSchnopFeatures[6]){
            if(e.band){
                return false;
            }
        }
        if(requiredSchnopFeatures[7]){
            if(!e.shadows){
                return false;
            }
        }
        if(requiredSchnopFeatures[8]){
            if(e.shadows){
                return false;
            }
        }
        if(requiredSchnopFeatures[9]){
            if(!e.shine){
                return false;
            }
        }
        if(requiredSchnopFeatures[10]){
            if(e.shine){
                return false;
            }
        }
        if(requiredSchnopFeatures[11]){
            if(!e.striped){
                return false;
            }
        }
        if(requiredSchnopFeatures[12]){
            if(e.striped){
                return false;
            }
        }
        if(requiredSchnopFeatures[13]){
            if(!e.crest){
                return false;
            }
        }
        if(requiredSchnopFeatures[14]){
            if(e.crest){
                return false;
            }
        }
        return true;
    }

    public void greenAndPurpleProtoTypes(float variance){
        generator.correlations[15][0] = variance;
        generator.correlations[15][1] = 0;
        generator.correlations[15][2] = 0;
        generator.correlations[15][3] = 0;
        generator.correlations[15][4] = variance;
        generator.correlations[15][5] = variance;
        generator.correlations[15][6] = 0;
        generator.correlations[15][7] = 0;
        generator.correlations[15][8] = variance;
        generator.correlations[15][9] = variance;
        generator.correlations[15][10] = 0;
        generator.correlations[15][11] = variance;
        generator.correlations[15][12] = 0;
        generator.correlations[15][13] = 0;
        generator.correlations[15][14] = variance;

        generator.correlations[16][0] = 0;
        generator.correlations[16][1] = 0;
        generator.correlations[16][2] = variance;
        generator.correlations[16][3] = variance;
        generator.correlations[16][4] = 0;
        generator.correlations[16][5] = 0;
        generator.correlations[16][6] = variance;
        generator.correlations[16][7] = variance;
        generator.correlations[16][8] = 0;
        generator.correlations[16][9] = 0;
        generator.correlations[16][10] = variance;
        generator.correlations[16][11] = 0;
        generator.correlations[16][12] = variance;
        generator.correlations[16][13] = 0;
        generator.correlations[16][14] = variance;
    }

    public Array<FlobbyOrSchnop> getIncorrectSchnops(Set<FlobbyOrSchnop> input){
        Array<FlobbyOrSchnop> output = new Array<FlobbyOrSchnop>();
        for (FlobbyOrSchnop e: input){
            if(!this.isItASchnop(e)){
                output.add(e);
            }
        }
        return output;
    }
}
