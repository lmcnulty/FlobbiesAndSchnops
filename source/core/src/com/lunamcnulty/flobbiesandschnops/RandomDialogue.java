package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.utils.Array;

import java.util.HashSet;

/**
 * Created by luna on 6/22/17.
 */

public class RandomDialogue {

    Array<String> dialogues;

    public RandomDialogue(){
        dialogues = new Array<String>();
        dialogues.add("The Devil's in the Daffodils.");
        dialogues.add("Nobody ever made any money climbing beanstocks.");
        dialogues.add("The sound at the beginning of 'Schnop' is the same as the one in words like 'Schnauzer' and 'Schnook'.");
        dialogues.add("The 'sh + n' sound is not really English. It's borrowed from languages like German and Yiddish.");
        dialogues.add("The name 'flobby' was inspired by floppy disks.");
        dialogues.add("Some games make you ask 'what is a game?' This one makes you ask 'What is a thing?'");
        dialogues.add("A winner is you.");
        dialogues.add("Fleens? You're not fleens!");
        dialogues.add("'Proprietary software keeps users divided and helpless.' -Richard Stallman");
        dialogues.add("Is 'undestroy' a word?");
        dialogues.add("Was Mufasa really such a great King?");
        dialogues.add("Install Gentoo.");
        dialogues.add("'Colorless green ideas sleep furiously.' -Noam Chomsky");
        dialogues.add("You are not expected to understand this.");
        dialogues.add("We do not use horse blubber in any of our products.");
        dialogues.add("No animals were harmed in the making of this game.");
        dialogues.add("'Please stop misattributing quotes to me' -Albert Einstein");
        dialogues.add("It's not a bug. It's a feature.");
    }

    public String get(){
        return dialogues.random();
    }
}
