package com.lunamcnulty.flobbiesandschnops;

import static com.badlogic.gdx.math.MathUtils.random;

/**
 * Created by luna on 6/7/17.
 */

public class FlobbyOrSchnopGenerator {

    float[][] correlations;
    float[] baseProportions;

    public FlobbyOrSchnopGenerator() {
        baseProportions = new float[10];
        for(int i = 0; i < 10; i++){
            baseProportions[i] = .5f;
        }
        baseProportions[0] = 1f/3f;
        baseProportions[1] = 1f/3f;
        baseProportions[2] = 1f/3f;

        correlations = new float[17][17];
        /* 0  = round
         * 1  = mixed
         * 2  = blocky
         * 3  = spotted
         * 4  = spotless
         * 5  = banded
         * 6  = bandless
         * 7  = shadowed
         * 8  = shadowless
         * 9  = shiny
         * 10 = shineless
         * 11 = striped
         * 12 = stripeless
         * 13 = crest
         * 14 = crestless
         * 15 = green
         * 16 = purple
         */
        for(int i = 0; i < 17; i++){
            for(int j = 0; j< 17; j++){
                correlations[i][j] = 0;
            }
        }
        correlations[0][8] = 1;
    }

    public int getOpposite(int property){
        switch(property){
            case 0:
                return 2;
            case 1:
                return 1;
            case 2:
                return 0;
            default:
                if(property % 2 == 0){
                    return property-1;
                }else{
                    return property+1;
                }
        }
    }
    public FlobbyOrSchnop generate(){
        FlobbyOrSchnop output = new FlobbyOrSchnop();
        if(random() < baseProportions[9]){
            output.color = FlobbyOrSchnop.Color.PURPLE;
        }else{
            output.color = FlobbyOrSchnop.Color.GREEN;
        }
        float bodyRoll = random();
        if(bodyRoll < baseProportions[0]){
            output.bodyType = FlobbyOrSchnop.BodyType.ROUND;
        }else if(bodyRoll < baseProportions[0]+baseProportions[1]){
            output.bodyType = FlobbyOrSchnop.BodyType.MIXED;
        } else if(bodyRoll < baseProportions[0]+baseProportions[1]+baseProportions[2]){
            output.bodyType = FlobbyOrSchnop.BodyType.BLOCKY;
        }
        if(random() < baseProportions[3]){
            output.spots = true;
        }else{
            output.spots = false;
        }
        if(random() < baseProportions[4]){
            output.band = true;
        }else{
            output.band = false;
        }
        if(random() < baseProportions[5]){
            output.shadows = true;
        }else{
            output.shadows = false;
        }
        if(random() < baseProportions[6]){
            output.shine = true;
        }else{
            output.shine = false;
        }
        if(random() < baseProportions[7]){
            output.striped = true;
        }else{
            output.striped = false;
        }
        if(random() < baseProportions[8]){
            output.crest = true;
        }else{
            output.crest = false;
        }
        if(output.spots){
            this.accountFor(3,output);
        }else{
            this.accountFor(4,output);
        }
        if(output.band){
            this.accountFor(5,output);
        }else{
            this.accountFor(6,output);
        }
        if(output.shadows){
            this.accountFor(7,output);
        }else{
            this.accountFor(8,output);
        }
        if(output.shine){
            this.accountFor(9,output);
        }else{
            this.accountFor(10,output);
        }
        if(output.striped){
            this.accountFor(11,output);
        }else{
            this.accountFor(12,output);
        }
        if(output.crest){
            this.accountFor(13,output);
        }else{
            this.accountFor(14,output);
        }
        if(output.color == FlobbyOrSchnop.Color.GREEN){
            this.accountFor(15,output);
        }else{
            this.accountFor(16,output);
        }
        if(output.bodyType == FlobbyOrSchnop.BodyType.MIXED){
            this.accountFor(1,output);
        }
        if(output.bodyType == FlobbyOrSchnop.BodyType.BLOCKY){
            this.accountFor(2,output);
        }
        if(output.bodyType == FlobbyOrSchnop.BodyType.ROUND){
            this.accountFor(0,output);
        }
        output.calculateTextureRegions();
        output.calculateShadowOffset();
        output.calculateHitRect();
        return output;
    }
    public void accountFor(int property,FlobbyOrSchnop output){
        if(random() < correlations[property][0]){
            output.bodyType = FlobbyOrSchnop.BodyType.ROUND;

        }
        if(random() < correlations[property][1]){
            output.bodyType = FlobbyOrSchnop.BodyType.MIXED;
        }
        if(random() < correlations[property][2]){
            output.bodyType = FlobbyOrSchnop.BodyType.BLOCKY;
        }
        if(random() < correlations[property][3]){
            output.spots = true;

        }
        if(random() < correlations[property][4]){
            output.spots = false;
        }

        if(random() < correlations[property][5]){
            output.band = true;
        }
        if(random() < correlations[property][6]){
            output.band = false;
        }
        if(random() < correlations[property][7]){
            output.shadows = true;
        }
        if(random() < correlations[property][8]){
            output.shadows = false;
        }
        if(random() < correlations[property][9]){
            output.shine = true;
        }
        if(random() < correlations[property][10]){
            output.shine = false;
        }
        if(random() < correlations[property][11]){
            output.striped = true;
        }
        if(random() < correlations[property][12]){
            output.striped = false;
        }
        if(random() < correlations[property][13]){
            output.crest = true;
        }
        if(random() < correlations[property][14]){
            output.crest = false;
        }
        if(random() < correlations[property][15]){
            output.color = FlobbyOrSchnop.Color.GREEN;
        }
        if(random() < correlations[property][16]){
            output.color = FlobbyOrSchnop.Color.PURPLE;
        }
    }
}
