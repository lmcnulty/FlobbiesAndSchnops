package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by luna on 6/10/17.
 */

public class Button {

    TextureRegion textureRegion;
    Viewport viewport;
    Vector2 position;
    Vector2 dimensions;
    Rectangle hitRect;
    float margin;
    int align;
    float scale;

    public Button(Viewport viewport, int align, TextureRegion textureRegion){
        this.align = align;
        this.textureRegion = textureRegion;
        margin = 25;
        this.viewport = viewport;
        position = new Vector2(0,0);

        scale = .65f;
        dimensions = new Vector2(textureRegion.getRegionWidth()*scale,textureRegion.getRegionHeight()*scale);
    }

    public void render(SpriteBatch batch, float delta){
        if(align == -1) {
            position.x = margin -viewport.getWorldWidth() / 2;
        }else{
            position.x = viewport.getWorldWidth()/2 - dimensions.x - margin;
        }
        position.y = viewport.getWorldHeight()/2 - dimensions.y - margin;
        hitRect = new Rectangle(position.x,position.y,dimensions.x,dimensions.y);
        Utils.drawTextureRegion(batch,textureRegion,position.x,position.y,scale);
    }
}
