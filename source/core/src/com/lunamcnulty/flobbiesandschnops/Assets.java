package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by luna on 6/6/17.
 */

public class Assets implements Disposable, AssetErrorListener {

    public static final String TAG = Assets.class.getName();
    public static final Assets instance = new Assets();
    private AssetManager assetManager;
    String location = "images/flobbiesandschnops.pack.atlas";
    BodyAssets bodyAssets;
    FeatureAssets featureAssets;
    MenuAssets menuAssets;
    HudAssets hudAssets;
    private Assets() {}

    public void init(AssetManager assetManager) {
        this.assetManager = assetManager;
        assetManager.setErrorListener(this);
        assetManager.load(location, TextureAtlas.class);
        assetManager.finishLoading();
        TextureAtlas atlas = assetManager.get(location);
        bodyAssets = new BodyAssets(atlas);
        featureAssets = new FeatureAssets(atlas);
        menuAssets = new MenuAssets(atlas);
        hudAssets = new HudAssets(atlas);
    }
    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(TAG, "Couldn't load asset: " + asset.fileName, throwable);
    }
    @Override
    public void dispose() {
        assetManager.dispose();
    }

    public class MenuAssets {
        public final TextureAtlas.AtlasRegion title;
        public final TextureAtlas.AtlasRegion musicButtonOn;
        public final TextureAtlas.AtlasRegion musicButtonOff;
        public MenuAssets(TextureAtlas atlas){
            title = atlas.findRegion("title-512");
            musicButtonOn = atlas.findRegion("music-button-on");
            musicButtonOff = atlas.findRegion("music-button-off");
        }
    }
    public class HudAssets{
        public final TextureAtlas.AtlasRegion submit;
        public final TextureAtlas.AtlasRegion menu;
        public final TextureAtlas.AtlasRegion panel;
        public final TextureAtlas.AtlasRegion textBox;
        public final TextureAtlas.AtlasRegion background;

        public HudAssets(TextureAtlas atlas){
            submit = atlas.findRegion("submit");
            menu = atlas.findRegion("menu");
            panel = atlas.findRegion("panel");
            textBox = atlas.findRegion("textbox");
            background = atlas.findRegion("background");
        }
    }

    public class BodyAssets {
        public final TextureAtlas.AtlasRegion bodyRoundGreen;
        public final TextureAtlas.AtlasRegion bodyRoundPurple;
        public final TextureAtlas.AtlasRegion bodyBlockyGreen;
        public final TextureAtlas.AtlasRegion bodyBlockyPurple;
        public final TextureAtlas.AtlasRegion bodyMixedGreen;
        public final TextureAtlas.AtlasRegion bodyMixedPurple;
        public final TextureAtlas.AtlasRegion blockyUnderShadow;
        public final TextureAtlas.AtlasRegion roundUnderShadow;
        public final TextureAtlas.AtlasRegion mixedUnderShadow;
        public BodyAssets(TextureAtlas atlas) {
            bodyRoundGreen = atlas.findRegion("body-round-green");
            bodyRoundPurple = atlas.findRegion("body-round-purple");
            bodyBlockyGreen = atlas.findRegion("body-blocky-green");
            bodyBlockyPurple = atlas.findRegion("body-blocky-purple");
            bodyMixedGreen = atlas.findRegion("body-mixed-green");
            bodyMixedPurple = atlas.findRegion("body-mixed-purple");
            blockyUnderShadow = atlas.findRegion("blocky-undershadow");
            roundUnderShadow = atlas.findRegion("round-undershadow");
            mixedUnderShadow = atlas.findRegion("mixed-undershadow");
        }
    }
    public class FeatureAssets {
        public final TextureAtlas.AtlasRegion spotsRound;
        public final TextureAtlas.AtlasRegion spotsBlocky;
        public final TextureAtlas.AtlasRegion spotsMixed;
        public final TextureAtlas.AtlasRegion crestRound;
        public final TextureAtlas.AtlasRegion crestBlocky;
        public final TextureAtlas.AtlasRegion crestMixed;
        public final TextureAtlas.AtlasRegion bandRound;
        public final TextureAtlas.AtlasRegion bandBlocky;
        public final TextureAtlas.AtlasRegion bandMixed;
        public final TextureAtlas.AtlasRegion stripedRound;
        public final TextureAtlas.AtlasRegion stripedBlocky;
        public final TextureAtlas.AtlasRegion stripedMixed;
        public final TextureAtlas.AtlasRegion shineRoundGreen;
        public final TextureAtlas.AtlasRegion shineRoundPurple;
        public final TextureAtlas.AtlasRegion shineMixedGreen;
        public final TextureAtlas.AtlasRegion shineMixedPurple;
        public final TextureAtlas.AtlasRegion shineBlockyGreen;
        public final TextureAtlas.AtlasRegion shineBlockyPurple;
        public final TextureAtlas.AtlasRegion shadowMixedGreen;
        public final TextureAtlas.AtlasRegion shadowMixedPurple;
        public final TextureAtlas.AtlasRegion shadowBlockyGreen;
        public final TextureAtlas.AtlasRegion shadowBlockyPurple;
        
        public FeatureAssets(TextureAtlas atlas){
            spotsRound = atlas.findRegion("spots-round");
            spotsBlocky = atlas.findRegion("spots-blocky");
            spotsMixed = atlas.findRegion("spots-mixed");
            crestRound = atlas.findRegion("crest-round");
            crestBlocky = atlas.findRegion("crest-blocky");
            crestMixed = atlas.findRegion("crest-mixed");
            bandRound = atlas.findRegion("band-round");
            bandBlocky = atlas.findRegion("band-blocky");
            bandMixed = atlas.findRegion("band-mixed");
            stripedRound = atlas.findRegion("striped-round");
            stripedBlocky = atlas.findRegion("striped-blocky");
            stripedMixed = atlas.findRegion("striped-mixed");
            shineRoundGreen = atlas.findRegion("shine-round-green");
            shineMixedGreen = atlas.findRegion("shine-mixed-green");
            shineMixedPurple = atlas.findRegion("shine-mixed-purple");
            shineRoundPurple = atlas.findRegion("shine-round-purple");
            shineBlockyPurple = atlas.findRegion("shine-blocky-purple");
            shineBlockyGreen = atlas.findRegion("shine-blocky-green");
            shadowMixedGreen = atlas.findRegion("shadows-mixed-green");
            shadowMixedPurple = atlas.findRegion("shadows-mixed-purple");
            shadowBlockyPurple = atlas.findRegion("shadows-blocky-purple");
            shadowBlockyGreen = atlas.findRegion("shadows-blocky-green");
        }
    }
}
