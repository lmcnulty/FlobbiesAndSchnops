package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by luna on 6/4/17.
 */

public class MenuFrame{

    TextFrame prompt;
    Vector2 position;
    Vector2 dimensions;
    float gapSize;
    GlyphLayout layout;
    String[] options;
    float optionPadding;
    boolean answered;
    int choice;
    int answer;
    Rectangle hitRect;

    public MenuFrame(TextFrame frame, String[] options){
        answered = false;
        prompt = frame;
        position = new Vector2(0,0);
        dimensions =  new Vector2(100,100);
        gapSize = 10;
        layout = new GlyphLayout();
        this.options = options;
        optionPadding = 200;
        choice = 0;
    }
    public void submitAnswer(){
        answer = choice;
        answered = true;
    }
    public void render(SpriteBatch batch, float delta){

        if(Gdx.input.isKeyJustPressed(Input.Keys.DOWN)){
            if(choice < options.length-1) {
                choice += 1;
            }
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.UP)){
            if(choice > 0) {
                choice -= 1;
            }
        }
        if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER)){
            this.submitAnswer();
        }

        if(options.length > 1) {
            float maxSize = Integer.MIN_VALUE;
            for(String e:options){
                layout.setText(prompt.font,e);
                float width = layout.width;
                if(width > maxSize){
                    maxSize = width;
                }
            }
            dimensions.x = maxSize+optionPadding+2*(prompt.textPadding+prompt.frameBorder);
            dimensions.y = (options.length*prompt.font.getLineHeight())+2*(prompt.frameBorder+prompt.textPadding);
            position.x = prompt.position.x + prompt.dimensions.x - dimensions.x;
            position.y = prompt.position.y+prompt.dimensions.y+gapSize;

            prompt.body.draw(batch, position.x, position.y, dimensions.x, dimensions.y);
            int linesDrawn = 0;
            while (linesDrawn < options.length) {

                prompt.font.draw(
                        batch,
                        options[linesDrawn],
                        position.x + prompt.frameBorder + prompt.textPadding + optionPadding,
                        position.y + prompt.frameBorder + prompt.textPadding + prompt.font.getLineHeight() * (options.length - linesDrawn)
                );
                prompt.font.draw(
                        batch,
                        "->",
                        position.x + prompt.frameBorder + prompt.textPadding,
                        position.y + prompt.frameBorder + prompt.textPadding + prompt.font.getLineHeight() * (options.length - choice)
                );
                linesDrawn += 1;
            }
        }
    }
}
