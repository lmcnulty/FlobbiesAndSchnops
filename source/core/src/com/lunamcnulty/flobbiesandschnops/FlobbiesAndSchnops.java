package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.Game;

public class FlobbiesAndSchnops extends Game {



    @Override
	public void create() {
        this.showLaunchScreen();
    }

    public void showLaunchScreen(){
        setScreen(new LaunchScreen(this));
    }

    public void showGameScreen(int level){
        setScreen(new FlobbiesAndSchnopsScreen(this,level));
    }
}