package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.DataBuffer;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by luna on 6/4/17.
 */

public class Dialogue {

    TextFrame frame;
    MenuFrame menu;
    Viewport viewport;
    String[] options;
    Dialogue[] followUp;
    boolean finished;
    Dialogue chosenFollowup;

    public Dialogue(Viewport viewport,String question, int responses){
        finished = false;
        frame = new TextFrame();
        options = new String[responses];
        followUp = new Dialogue[responses];
        menu = new MenuFrame(frame,options);
        this.viewport = viewport;
        frame.print(question);
    }

    public boolean isFinished(){
        boolean output = true;
        if(finished != true){
            output = false;
        }
        if(chosenFollowup != null && !chosenFollowup.isFinished()){
            output = false;
        }
        return output;
    }

    public void render(SpriteBatch batch, float delta){
        if(frame.printFinished && menu.answered ) {
            finished = true;
            if(followUp.length > 0 && followUp[menu.answer] != null) {
                chosenFollowup = followUp[menu.answer];
                chosenFollowup.render(batch,delta);
            }
        }
        frame.dimensions.x = viewport.getWorldWidth();
        frame.dimensions.y = viewport.getWorldHeight()/4;
        frame.position.x = viewport.getWorldWidth()/-2;
        frame.position.y = viewport.getWorldHeight()/-2;
        if(!frame.printFinished || !menu.answered) {
            frame.render(batch, delta);
        }
        if(frame.printFinished && !menu.answered) {
            menu.render(batch, delta);
        }
    }
    public void proceed(){
        if(frame.printFinished && menu.answered){
            if(chosenFollowup != null){
                chosenFollowup.proceed();
            }

        }else{
            if(!frame.printFinished){
                frame.proceed();
            }else if(!menu.answered){
                menu.submitAnswer();
            }
        }
    }

    public int getAnswer(){
        if (menu.answered){
            return menu.answer;
        }else{
            return -1;
        }
    }

    public MenuFrame getMenu(){
        if(frame.printFinished && menu.answered){
            if(chosenFollowup != null){
                return chosenFollowup.getMenu();
            }else{
                return null;
            }

        }else{
            return menu;
        }
    }
}

