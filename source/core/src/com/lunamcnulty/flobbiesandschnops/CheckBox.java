package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by luna on 6/17/17.
 */

public class CheckBox {

    String label;
    boolean checked;
    boolean showText;
    TextureRegion textureChecked;
    TextureRegion textureUnchecked;
    TextureRegion activeTextureRegion;
    Vector2 position;
    BitmapFont font;
    Rectangle hitRect;

    public CheckBox(String label, boolean checked, Vector2 position, TextureRegion textureChecked, TextureRegion textureUnchecked, boolean showText){
        this.label = label;
        this.checked = checked;
        this.position = position;
        this.textureChecked = textureChecked;
        this.textureUnchecked = textureUnchecked;
        activeTextureRegion = textureChecked;
        this.showText = showText;
        float width = activeTextureRegion.getRegionWidth();
        float height = activeTextureRegion.getRegionHeight();
        hitRect = new Rectangle(position.x,position.y,width,height);
        if(showText) {
            font = new BitmapFont(Gdx.files.internal("cantarell-mono.fnt"), false);
            font.setColor(Color.WHITE);
            font.getData().setScale(3f);
        }
    }
    public void toggle(){
        if(checked){
            checked = false;
            activeTextureRegion = textureUnchecked;
        }else{
            checked = true;
            activeTextureRegion = textureChecked;
        }
    }
    public void render(SpriteBatch batch, float delta){
        batch.begin();
        Utils.drawTextureRegion(batch,activeTextureRegion,position);
        if(showText) {
            font.draw(batch, label, position.x + 20, position.y + font.getLineHeight() / 4);
        }
        batch.end();
    }
}
