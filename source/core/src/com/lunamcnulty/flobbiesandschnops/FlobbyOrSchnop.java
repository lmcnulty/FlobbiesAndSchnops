package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by luna on 6/6/17.
 */

public class FlobbyOrSchnop {
    private Vector2 shadowOffset;
    Vector2 position;
    Vector2 destination;
    Vector2 velocity;
    float speed;
    Color color;
    BodyType bodyType;
    boolean spots;
    boolean crest;
    boolean band;
    boolean shadows;
    boolean shine;
    boolean striped;
    Rectangle hitRect;
    TextureRegion undershadowRegion;
    TextureRegion bodyRegion;
    TextureRegion spotsRegion;
    TextureRegion crestRegion;
    TextureRegion bandRegion;
    TextureRegion shadowsRegion;
    TextureRegion shineRegion;
    TextureRegion stripedRegion;

    public FlobbyOrSchnop(){
        position = new Vector2(0,0);
        destination = position;
        speed = 500;
        velocity = new Vector2(10,10);
        shadowOffset = new Vector2(0,0);
        color = Color.GREEN;
        bodyType = BodyType.MIXED;
        spots = false;
        crest = false;
        band = false;
        shadows = false;
        shine = false;
        striped = false;
    }

    public void calculateTextureRegions(){
        undershadowRegion = this.getUnderShadowTextureRegion();
        bodyRegion = this.getBodyTextureRegion();
        spotsRegion = this.getSpotsTextureRegion();
        crestRegion = this.getCrestTextureRegion();
        bandRegion = this.getBandTextureRegion();
        shadowsRegion = this.getShadowTextureRegion();
        shineRegion = this.getShineTextureRegion();
        stripedRegion = this.getStripedTextureRegion();
    }

    public void calculateShadowOffset(){
        shadowOffset = new Vector2(
                position.x-((undershadowRegion.getRegionWidth() - getBodyTextureRegion().getRegionWidth())/2)+(getBodyTextureRegion().getRegionWidth()/8),
                position.y-((undershadowRegion.getRegionHeight() - getBodyTextureRegion().getRegionHeight())/2)-(getBodyTextureRegion().getRegionHeight()/16)
        );
    }
    public void calculateHitRect(){
        hitRect = new Rectangle(
                position.x,
                position.y,
                bodyRegion.getRegionWidth(),
                bodyRegion.getRegionHeight()
        );
    }
    public void setPosition(Vector2 newPosition){
        position = newPosition;
        destination = newPosition;
    }
    public void setPositionX(float x){
        position.x = x;
        destination.x = x;
    }
    public void setPositionY(float y){
        position.y = y;
        destination.y = y;
    }
    public void render(SpriteBatch batch, float delta){
        if(position.x != destination.x || position.y != velocity.y) {
            velocity.x = delta * speed * (position.x - destination.x) / (float) Math.sqrt(Math.abs(Math.pow((position.x - destination.x), 2) + Math.pow((position.y - destination.y), 2)));
            velocity.y = delta * speed * (position.y - destination.y) / (float) Math.sqrt(Math.abs(Math.pow((position.x - destination.x), 2) + Math.pow((position.y - destination.y), 2)));

            if (Math.abs(velocity.x) < Math.abs(position.x - destination.x)) {
                position.x -= velocity.x;
            } else {
                position.x = destination.x;
            }
            if (velocity.y < (position.y - destination.y)) {
                position.y -= velocity.y;
            } else {
                position.y = destination.y;
            }
            calculateHitRect();
            calculateShadowOffset();
        }
        Utils.drawTextureRegion(batch, getUnderShadowTextureRegion(),shadowOffset.x,shadowOffset.y);
        Utils.drawTextureRegion(batch,getBodyTextureRegion(),position);
        if(shadows){
            Utils.drawTextureRegion(batch,shadowsRegion,position);
        }
        if(shine){
            Utils.drawTextureRegion(batch,shineRegion,position);
        }
        if(striped){
            Utils.drawTextureRegion(batch,stripedRegion,position);
        }
        if(spots){
            Utils.drawTextureRegion(batch,spotsRegion,position);
        }
        if(band){
            Utils.drawTextureRegion(batch,bandRegion,position);
        }
        if(crest){
            Utils.drawTextureRegion(batch,crestRegion,position);
        }
    }
    public TextureRegion getBodyTextureRegion(){
        switch(color){
            case GREEN:
                switch (bodyType){
                    case BLOCKY:
                        return Assets.instance.bodyAssets.bodyBlockyGreen;
                    case ROUND:
                        return Assets.instance.bodyAssets.bodyRoundGreen;
                    case MIXED:
                        return Assets.instance.bodyAssets.bodyMixedGreen;
                }
                break;
            case PURPLE:
                switch (bodyType){
                    case BLOCKY:
                        return Assets.instance.bodyAssets.bodyBlockyPurple;
                    case ROUND:
                        return Assets.instance.bodyAssets.bodyRoundPurple;
                    case MIXED:
                        return Assets.instance.bodyAssets.bodyMixedPurple;
                }
        }
        return Assets.instance.bodyAssets.bodyBlockyGreen;
    }
    public TextureRegion getSpotsTextureRegion(){
        switch (bodyType){
            case BLOCKY:
                return Assets.instance.featureAssets.spotsBlocky;
            case ROUND:
                return Assets.instance.featureAssets.spotsRound;
            case MIXED:
                return Assets.instance.featureAssets.spotsMixed;
        }
        return Assets.instance.featureAssets.spotsMixed;
    }
    public TextureRegion getCrestTextureRegion(){
        switch (bodyType){
            case BLOCKY:
                return Assets.instance.featureAssets.crestBlocky;
            case ROUND:
                return Assets.instance.featureAssets.crestRound;
            case MIXED:
                return Assets.instance.featureAssets.crestMixed;
        }
        return Assets.instance.featureAssets.crestMixed;
    }
    public TextureRegion getStripedTextureRegion(){
        switch (bodyType){
            case BLOCKY:
                return Assets.instance.featureAssets.stripedBlocky;
            case ROUND:
                return Assets.instance.featureAssets.stripedRound;
            case MIXED:
                return Assets.instance.featureAssets.stripedMixed;
        }
        return Assets.instance.featureAssets.stripedMixed;
    }
    public TextureRegion getBandTextureRegion(){
        switch (bodyType){
            case ROUND:
                return Assets.instance.featureAssets.bandRound;
            case MIXED:
                return Assets.instance.featureAssets.bandMixed;
        }
        return Assets.instance.featureAssets.bandMixed;
    }
    public TextureRegion getShineTextureRegion(){
        switch(color){
            case GREEN:
                switch (bodyType){
                    case BLOCKY:
                        return Assets.instance.featureAssets.shineBlockyGreen;
                    case ROUND:
                        return Assets.instance.featureAssets.shineRoundGreen;
                    case MIXED:
                        return Assets.instance.featureAssets.shineMixedGreen;
                }
                break;
            case PURPLE:
                switch (bodyType){
                    case BLOCKY:
                        return Assets.instance.featureAssets.shineBlockyPurple;
                    case ROUND:
                        return Assets.instance.featureAssets.shineRoundPurple;
                    case MIXED:
                        return Assets.instance.featureAssets.shineMixedPurple;
                }
        }
        return Assets.instance.featureAssets.shineBlockyGreen;
    }
    public TextureRegion getShadowTextureRegion(){
        switch(color){
            case GREEN:
                switch (bodyType){
                    case BLOCKY:
                        return Assets.instance.featureAssets.shadowBlockyGreen;
                    case MIXED:
                        return Assets.instance.featureAssets.shadowMixedGreen;
                }
                break;
            case PURPLE:
                switch (bodyType){
                    case BLOCKY:
                        return Assets.instance.featureAssets.shadowBlockyPurple;
                    case MIXED:
                        return Assets.instance.featureAssets.shadowMixedPurple;
                }
        }
        return Assets.instance.featureAssets.shadowBlockyGreen;
    }
    public TextureRegion getUnderShadowTextureRegion(){
        switch (bodyType){
            case BLOCKY:
                return Assets.instance.bodyAssets.blockyUnderShadow;
            case ROUND:
                return Assets.instance.bodyAssets.roundUnderShadow;
            case MIXED:
                return Assets.instance.bodyAssets.mixedUnderShadow;
        }
        return Assets.instance.bodyAssets.roundUnderShadow;
    }
    enum Color{GREEN,PURPLE}
    enum BodyType{ROUND,BLOCKY,MIXED}
}
