package com.lunamcnulty.flobbiesandschnops;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import static com.lunamcnulty.flobbiesandschnops.Constants.WORLD_SIZE;
import static com.lunamcnulty.flobbiesandschnops.Constants.prefs;

/**
 * Created by luna on 6/15/17.
 */

public class LaunchScreen extends InputAdapter implements Screen {

    FlobbiesAndSchnops game;
    ExtendViewport viewport;
    SpriteBatch batch;
    BitmapFont font;
    GlyphLayout layout;
    Menubutton startButton;
    Menubutton levelButton;
    int levelSelect;
    Menubutton plusButton;
    Menubutton minusButton;
    float buttonGap;
    CheckBox soundCheckBox;
    float titleScale;
    float margin;
    float buttonHeight;
    int highestAchievedLevel;

    public LaunchScreen(FlobbiesAndSchnops game){
        this.game = game;
        buttonGap = 50;
        buttonHeight = 400;
        titleScale = 3;
        margin = 100;
    }

    @Override
    public void show(){
        highestAchievedLevel = prefs.getInteger("level",1);
        Gdx.input.setInputProcessor(this);
        AssetManager am = new AssetManager();
        Assets.instance.init(am);
        levelSelect = 1;
        viewport = new ExtendViewport(Constants.WORLD_SIZE,Constants.WORLD_SIZE);
        font = new BitmapFont(Gdx.files.internal("cantarell-mono.fnt"),false);
        batch = new SpriteBatch();
        font.setColor(Color.WHITE);
        font.getData().setScale(1);
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        String startButtonText = "CONTINUE";
        if(prefs.getInteger("level") < 2){
            startButtonText = "NEW GAME";
        }
        float operatorButtonWidth = 200;
        startButton = new Menubutton(new Vector2(0,-200),new Vector2(1500,buttonHeight),startButtonText);
        plusButton = new Menubutton(
                new Vector2(
                        startButton.position.x - operatorButtonWidth/2 + startButton.dimensions.x/2,
                        startButton.position.y - startButton.dimensions.y/2 - buttonHeight/2 - buttonGap)
                ,new Vector2(operatorButtonWidth,buttonHeight),
                "+"
        );
        minusButton = new Menubutton(
                new Vector2(
                        startButton.position.x + operatorButtonWidth/2 - startButton.dimensions.x/2,
                        startButton.position.y - startButton.dimensions.y/2 - buttonHeight/2 - buttonGap)
                ,new Vector2(operatorButtonWidth,buttonHeight),
                "-"
        );
        levelButton = new Menubutton(
                new Vector2(startButton.position.x,startButton.position.y-startButton.dimensions.y - buttonGap),
                new Vector2(1000,buttonHeight),
                "PLAY LEVEL "+Integer.toString(levelSelect));
        soundCheckBox = new CheckBox(
                "SOUND",
                true,
                new Vector2(startButton.position.x + startButton.dimensions.x/2 + 300,0),
                Assets.instance.menuAssets.musicButtonOn,
                Assets.instance.menuAssets.musicButtonOff,
                false
                );
        if(prefs.getBoolean("sound",true) == false){
            soundCheckBox.toggle();
        }
    }

    @Override
    public void render(float delta) {
        viewport.apply();
        batch.setProjectionMatrix(viewport.getCamera().combined);
        Gdx.gl.glClearColor(.2f, .2f, .2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if(levelSelect < 2){
            minusButton.color = Color.DARK_GRAY;
        }else{
            minusButton.color = Color.GRAY;
        }
        if(levelSelect > highestAchievedLevel-1){
            plusButton.color = Color.DARK_GRAY;
        }else{
            plusButton.color = Color.GRAY;
        }
        startButton.render(batch,delta);
        levelButton.render(batch,delta);
        plusButton.render(batch,delta);
        minusButton.render(batch,delta);
        soundCheckBox.render(batch,delta);

        batch.begin();
        TextureRegion region = Assets.instance.menuAssets.title;
        Utils.drawTextureRegion(batch,region,-region.getRegionWidth()/2*titleScale,startButton.position.y + startButton.dimensions.y/2 +buttonGap*2,titleScale);
        batch.end();
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button){
        Vector2 touchPosition = viewport.unproject(new Vector2(screenX,screenY));
        if(startButton.hitRect.contains(touchPosition)){
            game.showGameScreen(highestAchievedLevel);
        }else if(levelButton.hitRect.contains(touchPosition)){
            game.showGameScreen(levelSelect);
        }else if(plusButton.hitRect.contains(touchPosition)){
            if(levelSelect < highestAchievedLevel) {
                levelSelect += 1;
                levelButton.text = "PLAY LEVEL " + Integer.toString(levelSelect);
            }
        }else if(minusButton.hitRect.contains(touchPosition)){
            if(levelSelect > 1) {
                levelSelect -= 1;
                levelButton.text = "PLAY LEVEL " + Integer.toString(levelSelect);
            }
        }else if(soundCheckBox.hitRect.contains(touchPosition)){
            soundCheckBox.toggle();
            if(soundCheckBox.checked){
                prefs.putBoolean("sound",true);
            }else{
                prefs.putBoolean("sound",false);
            }
            prefs.flush();

        }
        return true;
    }

    @Override
    public void resize(int width, int height){
        viewport.update(width,height);
    }

    @Override
    public void pause(){

    }

    @Override
    public void resume(){

    }

    @Override
    public void hide(){

    }

    @Override
    public void dispose () {
        batch.dispose();
        font.dispose();
        startButton.dispose();
    }
}
