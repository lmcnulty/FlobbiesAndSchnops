package com.lunamcnulty.flobbiesandschnops.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.lunamcnulty.flobbiesandschnops.FlobbiesAndSchnops;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(860, 640);
        }

        @Override
        public ApplicationListener createApplicationListener () {
                return new FlobbiesAndSchnops();
        }
}
